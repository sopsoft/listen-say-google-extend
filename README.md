# 听说

这是一个提供文章朗读的Chrome插件，解放双眼，聆听世界。

# 安装方式
* 1.下载本项目目录中的 listen-say-google.crx ，将文件拖动到Chrome浏览器的扩展界面。
* 2.下载项目代码，将代码目录拖入到Chrome浏览器的扩展界面中（需要先打开 “开发者模式”）

# 特性
* 对接了阿里云智能语音合成（tts），有多个发音人可选择
* 支持添加多个播放队列
* 支持微信公众号文章一键朗读
* 支持选定文本一键朗读
* 支持自定义文本内容朗读

# 说明
* 目前仅为测试版，如有建议或遇到bug，请通过邮箱 wangxx20@qq.com 联系我进行处理。


# 界面截图

![界面截图1](http://pan.52xyuguo.com/?/%E6%88%91%E7%9A%84%E8%BD%AF%E4%BB%B6/%E6%88%AA%E5%9B%BE/google-1.png)

![界面截图2](http://pan.52xyuguo.com/?/%E6%88%91%E7%9A%84%E8%BD%AF%E4%BB%B6/%E6%88%AA%E5%9B%BE/google-2.png)

![界面截图3](http://pan.52xyuguo.com/?/%E6%88%91%E7%9A%84%E8%BD%AF%E4%BB%B6/%E6%88%AA%E5%9B%BE/google-3.png)

![界面截图4](http://pan.52xyuguo.com/?/%E6%88%91%E7%9A%84%E8%BD%AF%E4%BB%B6/%E6%88%AA%E5%9B%BE/google-4.png)